import { Link } from "gatsby"
import PropTypes from "prop-types"
import navLogo from "../assets/img/nav-logo.png"
import "../assets/css/Navbar.css"
import React from "react"

const Header = ({ siteTitle }) => (
  <div id="Navbar" class="Navbar">
    <div class="brand">
      <img src={navLogo} class="brand-image" alt="logo"/>
    </div>
    <nav class="menubar">
      <ul class="menu-wrapper">
        <li class="menu-items"><Link to="/">Home</Link></li>
        <li class="menu-items"><Link to="/about/">About</Link></li>
        <li class="menu-items"><Link to="/gallery/">Gallery</Link></li>
        <li class="menu-items"><Link to="/projects/">Projects</Link></li>
        <li class="menu-items"><Link to="/achievements/">Achievements</Link></li>
      </ul>
    </nav>
  </div>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
