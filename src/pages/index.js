import React from "react"
import { Link } from "gatsby"
import titleLogo from "../assets/img/title-logo.png"
import titleImage from "../assets/img/title-image.png"
import "../assets/css/Index.css"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />

    <div id="Index" class="Index">
      <div class="index-wrapper">
        <div class="title-text">
            <img class="title-text-logo" src={titleLogo} alt="title-logo" />
            <h1 class="title-text-text">Hacker Space NSSCE</h1>
            <Link to="/about" class="title-text-btn">Know More</Link>
        </div>
        <div class="title-image">
          <img class="title-image-image" src={titleImage} alt="title" />
        </div>
      </div>
      <div class="motto">
        <div>
          Hack for life.
          <br />
          Hack for change.
          <br />
          Hack for better future.
          <br />
          <br />
          <br />
          <big>Let the coding culture prosper......</big>
        </div>
        <a href="/join" class="join-now">Join Now</a>
      </div>
    </div>
  </Layout>
)

export default IndexPage
